import java.util.Scanner;

public class Personal{
	
	public static void main(String[] args){

	Person person1 = new Person();
	Person person2 = new Person("Winston", 34);

	Scanner keyboard = new Scanner(System.in);
	
	System.out.println("person 1: " + person1.toString());
	System.out.println("person 2: " + person2.toString());

	System.out.println("\nEnter a name for person 1.");
	person1.setName(keyboard.next());
	System.out.println("person 1 name: " + person1.getName());

	System.out.println("\nEnter an age for person 1.");
	person1.setAge(keyboard.nextInt());
	System.out.println("person 1 age: " + person1.getAge());

	if(person1.getName().equals(person2.getName())){
		System.out.println("\nTheir names are the same.");
		}
	else{
		System.out.println("\nTheir names are different.");
		}
	if(person1.getAge() == person2.getAge()){
		System.out.println("Their ages are the same.");
		}
	if(person1.getAge() > person2.getAge()){
		System.out.println("person 1 is older than person 2.");
		}
	else{
		System.out.println("person 1 is younger than person 2.");
		}

	}
}
